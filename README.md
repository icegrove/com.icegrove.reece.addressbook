# Code Challenge - ReeceTech

## Build and run

Build jar:

`mvn clean package`

Run as jar:

`java -jar target/reece.addressbook-0.0.1-SNAPSHOT.jar`

Build docker image:

`docker build -t reece-ab .`

Run docker container:

`docker run -d --rm --name reece-ab -p 8080:8080 reece-ab`

## [API specification](http://localhost:8080/swagger-ui.html)

Endpoint

`GET /contacts`

may take ids of address books as parameters. For example:

`GET /contacts?ab=addressbook1&ab=addressbook2`

## H2 DB

[Console](http://localhost:8080/h2-console/)

JDBC URL: jdbc:h2:mem:addressbooks

Username: sa

Password: password