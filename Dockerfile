FROM openjdk:16-jdk
ADD target/reece.addressbook-0.0.1-SNAPSHOT.jar /opt/reece.addressbook-0.0.1-SNAPSHOT.jar
CMD exec java -jar /opt/reece.addressbook-0.0.1-SNAPSHOT.jar