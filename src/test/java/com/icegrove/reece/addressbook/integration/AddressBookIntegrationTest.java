package com.icegrove.reece.addressbook.integration;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import io.micrometer.core.instrument.util.IOUtils;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
class AddressBookIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    private String loadData(final String path) throws IOException {
        try(final InputStream is = AddressBookIntegrationTest.class.getResourceAsStream(path)) {
            return IOUtils.toString(is);
        }
    }

    @Test
    @Order(1)
    void testCreateAddressBook() throws Exception {
        mockMvc.perform(post("/addressbooks")
            .contentType(MediaType.APPLICATION_JSON)
            .content(loadData("/requests/createAddressBook.json")))
            .andExpect(status().isCreated());
    }

    @Test
    @Order(2)
    void testCreateContact() throws Exception {
        mockMvc.perform(post("/addressbooks/addressbook1/contacts")
            .contentType(MediaType.APPLICATION_JSON)
            .content(loadData("/requests/createContact.json")))
            .andExpect(status().isCreated());
    }

    @Test
    @Order(3)
    void testGetContactSuccess() throws Exception {
        mockMvc.perform(get("/addressbooks/addressbook1/contacts/1"))
            .andExpect(status().isOk());
    }

    @Test
    @Order(4)
    void testGetContact404() throws Exception {
        mockMvc.perform(get("/addressbooks/addressbook1/contacts/1000"))
            .andExpect(status().isNotFound());
    }

    @Test
    @Order(5)
    void testDeleteContactSuccess() throws Exception {
        mockMvc.perform(delete("/addressbooks/addressbook1/contacts/1"))
            .andExpect(status().isNoContent());
    }

    @Test
    @Order(6)
    void testDeleteContactAgainSuccess() throws Exception {
        mockMvc.perform(delete("/addressbooks/addressbook1/contacts/1"))
            .andExpect(status().isNoContent());
    }

    @Test
    @Order(7)
    void testDeleteAddressBookSuccess() throws Exception {
        mockMvc.perform(delete("/addressbooks/addressbook1"))
            .andExpect(status().isNoContent());
    }

    @Test
    @Order(8)
    void testDeleteAddressBookAgainSuccess() throws Exception {
        mockMvc.perform(delete("/addressbooks/addressbook1"))
            .andExpect(status().isNoContent());
    }

}
