package com.icegrove.reece.addressbook.fixtures;

import com.icegrove.reece.addressbook.model.Contact;

public class ContactFixtures {

    public static Contact newContact1() {
        final Contact contact = new Contact();

        contact.setFirstName("John");
        contact.setLastName("Smith");
        contact.setHomePhone("0355776611");
        contact.setMobile("0426843954");
        contact.setWorkPhone("0388662244");

        return contact;
    }

    public static Contact newContact2() {
        final Contact contact = new Contact();

        contact.setFirstName("John");
        contact.setLastName("Doe");
        contact.setHomePhone("0333779955");
        contact.setMobile("0486495732");
        contact.setWorkPhone("0344662288");

        return contact;
    }

    public static Contact existingContact1() {
        final Contact contact = newContact1();

        contact.setId(1);
        contact.setAddressBook(AddressBookFixtures.existingAddressBook1());

        return contact;
    }

    public static Contact existingContact2() {
        final Contact contact = newContact2();

        contact.setId(2);
        contact.setAddressBook(AddressBookFixtures.existingAddressBook2());

        return contact;
    }

}
