package com.icegrove.reece.addressbook.fixtures;

import com.icegrove.reece.addressbook.model.AddressBook;

public class AddressBookFixtures {

    public static AddressBook newAddressBook1() {
        final AddressBook addressBook = new AddressBook();

        addressBook.setName("Address Book 1");

        return addressBook;
    }

    public static AddressBook existingAddressBook1() {
        final AddressBook addressBook = new AddressBook();

        addressBook.setId("ab1");
        addressBook.setName("AB1");

        return addressBook;
    }

    public static AddressBook existingAddressBook2() {
        final AddressBook addressBook = new AddressBook();

        addressBook.setId("ab2");
        addressBook.setName("AB2");

        return addressBook;
    }

}
