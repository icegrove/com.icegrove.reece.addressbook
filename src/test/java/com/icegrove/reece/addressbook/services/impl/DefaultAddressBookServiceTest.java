package com.icegrove.reece.addressbook.services.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.icegrove.reece.addressbook.dao.AddressBookRepository;
import com.icegrove.reece.addressbook.fixtures.AddressBookFixtures;
import com.icegrove.reece.addressbook.model.AddressBook;
import com.icegrove.reece.addressbook.services.BadDataException;

@ExtendWith(MockitoExtension.class)
class DefaultAddressBookServiceTest {

    private DefaultAddressBookService service;

    @Mock
    private AddressBookRepository addressBookRepository;

    @BeforeEach
    void init() {
        service = new DefaultAddressBookService();
        ReflectionTestUtils.setField(service, "addressBookRepository", addressBookRepository);
    }

    @Captor
    private ArgumentCaptor<AddressBook> addressBookCaptor;    

    @Test
    void testCreateSuccessfull() throws BadDataException {
        final AddressBook addressBook = AddressBookFixtures.newAddressBook1();
        final String id = "addressbook1";

        when(addressBookRepository.existsById(id)).thenReturn(Boolean.FALSE);
        when(addressBookRepository.save(addressBookCaptor.capture())).thenReturn(addressBook);

        final AddressBook actualAddressBook = service.createAddressBook(addressBook);

        assertThat(addressBookCaptor.getValue(), hasProperty("id", is(id)));
        assertThat(actualAddressBook, is(addressBook));
    }

    @Test
    void testCreateAlreadyExists() throws BadDataException {
        final AddressBook addressBook = AddressBookFixtures.newAddressBook1();
        final String id = "addressbook1";

        when(addressBookRepository.existsById(id)).thenReturn(Boolean.TRUE);

        assertThrows(BadDataException.class, () -> service.createAddressBook(addressBook));

        verify(addressBookRepository, never()).save(any());
    }

    @Test
    void testDeleteSuccessfull() {
        final String addressBookId = "ab";

        service.deleteAddressBook(addressBookId);

        verify(addressBookRepository, times(1)).deleteById(addressBookId);
    }

}
