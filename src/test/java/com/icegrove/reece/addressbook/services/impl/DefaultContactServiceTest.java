package com.icegrove.reece.addressbook.services.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.icegrove.reece.addressbook.dao.AddressBookRepository;
import com.icegrove.reece.addressbook.dao.ContactRepository;
import com.icegrove.reece.addressbook.fixtures.AddressBookFixtures;
import com.icegrove.reece.addressbook.fixtures.ContactFixtures;
import com.icegrove.reece.addressbook.model.AddressBook;
import com.icegrove.reece.addressbook.model.Contact;
import com.icegrove.reece.addressbook.services.NotFoundException;

@ExtendWith(MockitoExtension.class)
class DefaultContactServiceTest {

    private DefaultContactService service;

    @Mock
    private AddressBookRepository addressBookRepository;
    @Mock
    private ContactRepository contactRepository;

    @BeforeEach
    void init() {
        service = new DefaultContactService();
        ReflectionTestUtils.setField(service, "addressBookRepository", addressBookRepository);
        ReflectionTestUtils.setField(service, "contactRepository", contactRepository);
    }

    @Captor
    private ArgumentCaptor<Set<String>> idsCaptor;    

    @Test
    void testCreateSuccess() throws NotFoundException {
        final Integer contactId = 1;
        final AddressBook addressBook = AddressBookFixtures.existingAddressBook1();

        final Contact contact = ContactFixtures.newContact1();

        when(addressBookRepository.findById(addressBook.getId())).thenReturn(Optional.of(addressBook));

        when(contactRepository.save(contact)).thenAnswer(invocation -> {
            final Contact c = invocation.getArgument(0);
            c.setId(contactId);
            c.setAddressBook(addressBook);
            return c;
        });

        service.createContact(addressBook.getId(), contact);

        assertThat(contact, allOf(hasProperty("id", is(contactId)), hasProperty("addressBook", is(addressBook))));
    }

    @Test
    void testCreateAddressBookNotFound() throws NotFoundException {
        final String addressBookId = "not-exist";
        final Contact contact = ContactFixtures.newContact1();

        when(addressBookRepository.findById(addressBookId)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> service.createContact(addressBookId, contact));

        verify(contactRepository, never()).save(any());
    }

    @Test
    void testDeleteSuccess() {
        final Contact contact = ContactFixtures.existingContact1();

        when(contactRepository.findById(contact.getId())).thenReturn(Optional.of(contact));

        service.deleteContact(contact.getAddressBook().getId(), contact.getId());

        verify(contactRepository, times(1)).delete(contact);
    }

    @Test
    void testDeleteWrongAddressBook() {
        final Contact contact = ContactFixtures.existingContact1();

        when(contactRepository.findById(contact.getId())).thenReturn(Optional.of(contact));

        service.deleteContact("not-exists", contact.getId());

        verify(contactRepository, never()).delete(any());
    }

    @Test
    void testGetContactsInAddressBookSuccess() throws IOException {
        final Contact contact = ContactFixtures.existingContact1();
        final String addressBookId = contact.getAddressBook().getId();

        when(contactRepository.findByAddressBookIds(idsCaptor.capture())).thenReturn(Collections.singletonList(contact));

        final List<Contact> actualContacts = service.getContacts(addressBookId);

        assertThat(idsCaptor.getValue(), contains(addressBookId));
        assertThat(actualContacts, contains(contact));
    }

    @Test
    void testGetContactsInAddressBooksSuccess() {
        final List<Contact> contacts = List.of(ContactFixtures.existingContact1(), ContactFixtures.existingContact2());
        final Set<String> addressBookIds = contacts.stream()
                .map(c -> c.getAddressBook().getId())
                .collect(Collectors.toSet());

        when(contactRepository.findByAddressBookIds(idsCaptor.capture())).thenReturn(contacts);

        final List<Contact> actualContacts = service.getContacts(addressBookIds);

        assertThat(idsCaptor.getValue(), is(addressBookIds));
        assertThat(actualContacts, is(contacts));
    }

}
