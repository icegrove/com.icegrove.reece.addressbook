package com.icegrove.reece.addressbook.services;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.icegrove.reece.addressbook.model.Contact;

public interface ContactService {

    List<Contact> getContacts(String addressBookId);

    List<Contact> getContacts(Set<String> addressBookIds);

    Optional<Contact> getContact(String addressBookId, int contactId);

    void createContact(String addressBookId, Contact contact) throws NotFoundException;

    void deleteContact(String addressBookId, int contactId);

}
