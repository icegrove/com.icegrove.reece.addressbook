package com.icegrove.reece.addressbook.services;

import java.util.List;
import java.util.Optional;

import com.icegrove.reece.addressbook.model.AddressBook;

public interface AddressBookService {

    List<AddressBook> getAddressBooks();

    Optional<AddressBook> getAddressBook(String addressBookId);

    AddressBook createAddressBook(AddressBook addressBook) throws BadDataException;

    void deleteAddressBook(String addressBookId);

}
