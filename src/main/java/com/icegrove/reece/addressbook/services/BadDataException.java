package com.icegrove.reece.addressbook.services;

public class BadDataException extends ServiceException {

    private static final long serialVersionUID = 1L;

    public BadDataException() {
        super();
    }

    public BadDataException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BadDataException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadDataException(String message) {
        super(message);
    }

    public BadDataException(Throwable cause) {
        super(cause);
    }

}
