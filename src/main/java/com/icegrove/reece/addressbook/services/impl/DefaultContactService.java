package com.icegrove.reece.addressbook.services.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.icegrove.reece.addressbook.dao.AddressBookRepository;
import com.icegrove.reece.addressbook.dao.ContactRepository;
import com.icegrove.reece.addressbook.model.AddressBook;
import com.icegrove.reece.addressbook.model.Contact;
import com.icegrove.reece.addressbook.services.ContactService;
import com.icegrove.reece.addressbook.services.NotFoundException;

@Service
@Transactional
public class DefaultContactService implements ContactService {

    @Autowired
    private AddressBookRepository addressBookRepository;
    @Autowired
    private ContactRepository contactRepository;

    @Override
    public List<Contact> getContacts(final String addressBookId) {
        return getContacts(Collections.singleton(addressBookId));
    }

    @Override
    public List<Contact> getContacts(final Set<String> addressBookIds) {
        // If address book ids not specified return everything,
        return addressBookIds.isEmpty() ? contactRepository.findAll() : contactRepository.findByAddressBookIds(addressBookIds);
    }

    @Override
    public Optional<Contact> getContact(final String addressBookId, final int contactId) {
        return contactRepository.findByAddressBookIdAndContactId(addressBookId, contactId);
    }

    @Override
    public void createContact(final String addressBookId, final Contact contact) throws NotFoundException {
        final AddressBook addressBook = addressBookRepository.findById(addressBookId).orElseThrow(() -> new NotFoundException("Address book not found."));
        contact.setAddressBook(addressBook);

        contactRepository.save(contact);
    }

    @Override
    public void deleteContact(final String addressBookId, final int contactId) {
        contactRepository.findById(contactId)
            .filter(c -> c.getAddressBook().getId().equals(addressBookId))
            .ifPresent(c -> contactRepository.delete(c));
    }

}
