package com.icegrove.reece.addressbook.services.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.icegrove.reece.addressbook.dao.AddressBookRepository;
import com.icegrove.reece.addressbook.model.AddressBook;
import com.icegrove.reece.addressbook.services.AddressBookService;
import com.icegrove.reece.addressbook.services.BadDataException;

@Service
public class DefaultAddressBookService implements AddressBookService {

    @Autowired
    private AddressBookRepository addressBookRepository;

    private String generateAddressBookId(final AddressBook addressBook) {
        // Converting name to URL path segment.
        return addressBook.getName().toLowerCase().replaceAll("\\W", "");
    }

    @Override
    public List<AddressBook> getAddressBooks() {
        return addressBookRepository.findAll();
    }

    @Override
    public Optional<AddressBook> getAddressBook(String addressBookId) {
        return addressBookRepository.findById(addressBookId);
    }

    @Override
    @Transactional
    public AddressBook createAddressBook(final AddressBook addressBook) throws BadDataException {
        final String id = generateAddressBookId(addressBook);

        if(addressBookRepository.existsById(id)) {
            throw new BadDataException("Already exists");
        }

        addressBook.setId(id);
        return addressBookRepository.save(addressBook);
    }

    @Override
    public void deleteAddressBook(final String addressBookId) {
        try {
            addressBookRepository.deleteById(addressBookId);
        } catch (final EmptyResultDataAccessException e) {
            // Should be idempotent
        }
    }

}
