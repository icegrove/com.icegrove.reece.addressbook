package com.icegrove.reece.addressbook.controllers;

import com.icegrove.reece.addressbook.model.Contact;

public class ContactDto {

    private String firstName;
    private String lastName;
    private String homePhone;
    private String mobile;
    private String workPhone;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getHomePhone() {
        return homePhone;
    }
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }
    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
    public String getWorkPhone() {
        return workPhone;
    }
    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public Contact toContact() {
        final Contact contact = new Contact();

        contact.setFirstName(firstName);
        contact.setLastName(lastName);
        contact.setHomePhone(homePhone);
        contact.setMobile(mobile);
        contact.setWorkPhone(workPhone);

        return contact;
    }

}
