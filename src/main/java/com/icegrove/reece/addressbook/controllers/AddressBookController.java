package com.icegrove.reece.addressbook.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.icegrove.reece.addressbook.model.AddressBook;
import com.icegrove.reece.addressbook.services.AddressBookService;
import com.icegrove.reece.addressbook.services.BadDataException;
import com.icegrove.reece.addressbook.services.NotFoundException;

@RestController
@RequestMapping(value = "addressbooks", produces = MediaType.APPLICATION_JSON_VALUE)
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public List<AddressBook> getAddressBooks() {
        return addressBookService.getAddressBooks();
    }

    @GetMapping("{addressBookId}")
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public AddressBook getAddressBook(@PathVariable final String addressBookId) throws NotFoundException {
        return addressBookService.getAddressBook(addressBookId).orElseThrow(() -> new NotFoundException("Not found"));
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public AddressBook createAddressBook(@RequestBody final AddressBookDto addressBookDto) throws BadDataException {
        final AddressBook addressBook = new AddressBook();
        addressBook.setName(addressBookDto.getName());

        return addressBookService.createAddressBook(addressBook);
    }

    @DeleteMapping("{addressBookId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteAddressBook(@PathVariable final String addressBookId) {
        addressBookService.deleteAddressBook(addressBookId);
    }

}
