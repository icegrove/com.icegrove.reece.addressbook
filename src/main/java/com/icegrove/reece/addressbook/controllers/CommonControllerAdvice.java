package com.icegrove.reece.addressbook.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.icegrove.reece.addressbook.services.BadDataException;
import com.icegrove.reece.addressbook.services.NotFoundException;

@ControllerAdvice
public class CommonControllerAdvice {

    private static final Logger logger = LoggerFactory.getLogger(CommonControllerAdvice.class);

    @ExceptionHandler({BadDataException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Error errorHandling(final BadDataException exception) {
        final String message = exception.getMessage();
        logger.info(message, exception);

        final Error error = new Error();
        error.setMessage(message);
        return error;
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Error errorHandling(final NotFoundException exception) {
        final String message = exception.getMessage();
        logger.info(message, exception);

        final Error error = new Error();
        error.setMessage(message);
        return error;
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public Error errorHandling(final Exception exception) {
        final String message = exception.getMessage();
        logger.info(message, exception);

        final Error error = new Error();
        error.setMessage(message);
        return error;
    }

}
