package com.icegrove.reece.addressbook.controllers;

public class AddressBookDto {

    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

}
