package com.icegrove.reece.addressbook.controllers;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.icegrove.reece.addressbook.model.Contact;
import com.icegrove.reece.addressbook.services.ContactService;
import com.icegrove.reece.addressbook.services.NotFoundException;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class ContactController {

    @Autowired
    private ContactService contactService;

    @GetMapping("addressbooks/{addressBookId}/contacts")
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public List<Contact> getContacts(@PathVariable final String addressBookId) {
        return contactService.getContacts(addressBookId);
    }

    @GetMapping("addressbooks/{addressBookId}/contacts/{contactId}")
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public Contact getContact(@PathVariable final String addressBookId, @PathVariable final int contactId) throws NotFoundException {
        return contactService.getContact(addressBookId, contactId).orElseThrow(() -> new NotFoundException("Not found"));
    }

    @DeleteMapping("addressbooks/{addressBookId}/contacts/{contactId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteContact(@PathVariable final String addressBookId, @PathVariable final int contactId) {
        contactService.deleteContact(addressBookId, contactId);
    }

    @GetMapping("contacts")
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    public List<Contact> getContacts(@RequestParam(name = "ab", required = false) final Set<String> addressBookIds) {
        return contactService.getContacts(addressBookIds == null ? Collections.emptySet() : addressBookIds);
    }

    @PostMapping(value = "addressbooks/{addressBookId}/contacts")
    @ResponseStatus(value = HttpStatus.CREATED)
    @ResponseBody
    public Contact createContact(@PathVariable final String addressBookId, @RequestBody ContactDto contactDto) throws NotFoundException {
        final Contact contact = contactDto.toContact();
        contactService.createContact(addressBookId, contact);

        return contact;
    }

}
