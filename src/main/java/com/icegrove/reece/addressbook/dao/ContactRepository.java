package com.icegrove.reece.addressbook.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.icegrove.reece.addressbook.model.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {

    @Query("SELECT c FROM Contact c WHERE c.addressBook.id IN ?1")
    List<Contact> findByAddressBookIds(Set<String> addressBookIds);

    @Query("SELECT c FROM Contact c WHERE c.addressBook.id = ?1 AND c.id = ?2")
    Optional<Contact> findByAddressBookIdAndContactId(String addressBookId, int contactId);

}
