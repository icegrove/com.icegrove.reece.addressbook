package com.icegrove.reece.addressbook.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.icegrove.reece.addressbook.model.AddressBook;

@Repository
public interface AddressBookRepository extends JpaRepository<AddressBook, String> {
}
